# <one line to give the program's name and a brief idea of what it does.>
# Copyright (C) 2016  Matt McAninch
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import re

f = open("input.txt", 'r')
badWords = ['ab', 'cd', 'pq', 'xy']
badFlag = False
goodWord = 0

#regx = re.compile('(\w)\1')
for lines in f.readlines():
    vowelCount = 0
    badFlag = False
    #Rule checks
    #checks for 3 vowels

    for letters in lines:
        if letters in ('a', 'e', 'i', 'o', 'u'):
            vowelCount += 1

    if vowelCount < 3:
        badFlag = True


    for each in badWords:
        if lines.find(each) != -1:
           # print("badWords: ", lines[lines.find(each): len(lines)])
            badFlag = True
            break
    regx = re.search(r'(\w)\1', lines)

    #if regx.search(lines) is None:
    #    print(regx.search(lines))
    if regx is None:
        #print(regx)
        badFlag = True

    if badFlag is False:
        goodWord += 1
        print(regx)
        print("lines good: ", lines)

print("total: ", goodWord)