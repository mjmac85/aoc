# <one line to give the program's name and a brief idea of what it does.>
# Copyright (C) 2016  Matt McAninch
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import re

f = open("input.txt", 'r')
goodWord = 0

#regx = re.compile('(\w)\1')
for lines in f.readlines():
    regx = re.search(r'(\w).\1', lines)
    #print(regx)


    #print(lines, "    -    ", regx2)                  # ((\w)\w)[^\2].*\1
    if regx is not None:
        regx2 = re.search(r'((\w)\w).*\1', lines)
        if regx2 is not None:
            goodWord += 1
    # if regx is not None:
    #     print(lines, regx)
    #     goodWord += 1



        # print(regx)
        # print("lines good: ", lines)

print("total: ", goodWord)