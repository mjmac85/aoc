# <one line to give the program's name and a brief idea of what it does.>
# Copyright (C) 2016  Matt McAninch
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
f = open('input.txt', 'r')
list1 = []
total = 0
bow = 0
for line in f.readlines():
    list1 = line.split("x")
    list2 = sorted([float(i) for i in list1])
    #Bow
    bow += ((list2[0] * 2) + (list2[1] * 2) + (list2[0] * list2[1] * list2[2]))



    #wrapping paper
    #list2 = sorted(list2)
    #list2 = [2*i for i in list2]
    total += ((2 * (list2[0] * list2[1])) + (2 * (list2[2] * list2[1]))
             + (2 * (list2[0] * list2[2]))) + (list2[0] * list2[1])

    #print(area)

    #total += sum(list2) + min(list2)
     


    #print(min(list2))
    #print(list2)
    #print("h=%s, w=%s, l=%s" % (h, w, l))
#print(total)


print(bow)
print(total)
f.close()