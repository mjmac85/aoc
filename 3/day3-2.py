# <one line to give the program's name and a brief idea of what it does.>
# Copyright (C) 2016  Matt McAninch
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import numpy as np
f = open('input.txt', 'r')
directions = f.read()
#controls up and down
y = 0
y2 = 0
#x left and right
x = 0
x2 = 0
#print(grid[str(x) + str(y)])

grid = {(str(y) + '|' + str(x)): 1}

count = 0


santa = 0
for each in directions:
    if santa % 2 == 0:
        if each == 'v':
            y2 -= 1
            if (str(y2) + '|' + str(x2)) in grid.keys():
                grid[str(y2) + '|' + str(x2)] += 1
            else:
                grid[str(y2) + '|' + str(x2)] = 1
        if each == '^':
            y2 += 1
            if (str(y2) + '|' + str(x2)) in grid.keys():
                grid[str(y2) + '|' + str(x2)] += 1
            else:
                grid[str(y2) + '|' + str(x2)] = 1
        if each == '>':
            x2 += 1
            if (str(y2) + '|' + str(x2)) in grid.keys():
                grid[str(y2) + '|' + str(x2)] += 1
            else:
                grid[str(y2) + '|' + str(x2)] = 1
        if each == '<':
            x2 -= 1
            if (str(y2) + '|' + str(x2)) in grid.keys():
                grid[str(y2) + '|' + str(x2)] += 1
            else:
                grid[str(y2) + '|' + str(x2)] = 1




    else:
        if each == 'v':
            y -= 1
            if (str(y) + '|' + str(x)) in grid.keys():
                grid[str(y) + '|' + str(x)] += 1
            else:
                grid[str(y) + '|' + str(x)] = 1
        if each == '^':
            y += 1
            if (str(y) + '|' + str(x)) in grid.keys():
                grid[str(y) + '|' + str(x)] += 1
            else:
                grid[str(y) + '|' + str(x)] = 1
        if each == '>':
            x += 1
            if (str(y) + '|' + str(x)) in grid.keys():
                grid[str(y) + '|' + str(x)] += 1
            else:
                grid[str(y) + '|' + str(x)] = 1
        if each == '<':
            x -= 1
            if (str(y) + '|' + str(x)) in grid.keys():
                grid[str(y) + '|' + str(x)] += 1
            else:
                grid[str(y) + '|' + str(x)] = 1
    santa += 1

#counts up the total
for each in grid.keys():
    if grid[each] >= 1:
        count += 1
print("count: ", count)
f.close()